# Creative Coding Zusammenfassung

## Abgaben
Größere Projekte welche auf den Übungen aufbauen.

### Shader Metaballs
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
![Language](https://img.shields.io/badge/Language-GLSL-086703?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-js-shader-metacircles){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-js-shader-metacircles/){target=_blank}  

Implementierung von Metaballs mithilfe von TypeScript, p5.js und WebGL.  

![](./projects/img/2022-01-09-05-01-49.png)
[Mehr Infos :octicons-chevron-right-16:](./projects/shader-metaballs.md){.md-button .md-btn-small-pad}

### Heightmap
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-heightmap){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-heightmap/){target=_blank}  

Wandelt eine Höhenkarte in ein Bild, welches an Joy Divisions "Unknown Pleasures" Album-Cover angelehnt ist.

![](./projects/img/2022-01-09-03-02-34.png)

[Mehr Infos :octicons-chevron-right-16:](./projects/heightmap.md){.md-button .md-btn-small-pad}

### Shader Heightmap
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
![Language](https://img.shields.io/badge/Language-GLSL-086703?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-js-shader-heightmap){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-js-shader-heightmap/){target=_blank}  

Eine Variante der [Heightmap](#heightmap) welche mit P5 im WebGL-Modus implementiert wurde.

![](2022-01-24-03-25-24.png)

[Mehr Infos :octicons-chevron-right-16:](./projects/heightmap-shader.md){.md-button .md-btn-small-pad}



## Übungen

### Hello World
![Language](https://img.shields.io/badge/Language-Java-b07219?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/hello-world){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Video-red?style=for-the-badge&logo=youtube)](https://www.youtube.com/watch?v=rQdVxqNbHU0){target=_blank}

Eine kleine Anwendung welche Partikel in einer Spirale Erzeugt. Die Farbe und Größe des Partikels sind an die Lebenszeit des Partikels gebunden.

![](https://i.imgur.com/K8hi7zK.png)

### Space Invaders
![Language](https://img.shields.io/badge/Language-Java-b07219?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/space-invaders){target=_blank}

Eine Anwendung welche aus einem Eingabe-Bild ein Mosiak aus zufälligen Invadern erzeugt.

[Mehr Infos :octicons-chevron-right-16:](./projects/spaceinvaders.md){.md-button .md-btn-small-pad}


### Quarter Circles
![Language](https://img.shields.io/badge/Language-Java-b07219?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/quarter-circles){target=_blank}

Erzeugt eine Bild aus Viertelkreisen. Die Größe hängt dabei vom Mauszeiger ab.

![](https://i.imgur.com/5aYW1H8.png)

[Mehr Infos :octicons-chevron-right-16:](./projects/quartercircles.md){.md-button .md-btn-small-pad}

### Particles
![Language](https://img.shields.io/badge/Language-Java-b07219?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/particles){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Video-red?style=for-the-badge&logo=youtube)](https://youtu.be/xaRbiuhVwyQ){target=_blank}



Erzeugt ein pixeliertes Feuer mithilfe von Partikeln. Die Größe, Farbe und Position hängen von der Lebenszeit des Partikels ab.

![](https://i.imgur.com/6djUqDP.png)  
[Mehr Infos :octicons-chevron-right-16:](./projects/particles.md){.md-button .md-btn-small-pad}

### Moving Circles
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/moving-circles-ts){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/moving-circles-ts/){target=_blank}  

Erzeugt ein Bild bei welchem Kreise, Abhängig von der Mausposition, in zufällige Richtungen verschoben werden.

![](2022-01-08-23-11-07.png)

### Scale Rotate Quads
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/scale-rotate-quads){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/scale-rotate-quads/){target=_blank}  

Kleine Animation bei welcher Quadrate in eine zufällig Richtung rotiert, skaliert und transformiert werden. 

![](https://i.imgur.com/IB0S6lr.png)

### Kaleidoscope
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/kaleidoscope){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/kaleidoscope/){target=_blank}  

Web-App welche das Malen eines Kaleidoskops ermöglicht. Durch das Drücken der linken Maustaste wird an der Mauszeigerposition, sowie an allen anderen "Armen" des Bildes gemalt.

![](img/2022-01-08-17-21-27.png)
### 2D Flowfield
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p5-2d-flowfield){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p5-2d-flowfield/){target=_blank}  

Demonstration einer Flowfield-Implementierung. Ein Agent möchte immer zum Mauszeiger gelangen.

Über einen Linksklick kann ein Hindernis hinzugefügt werden. Das Klicken außerhalb des Canvas löscht alle Hindernisse.

![](./img/2022-01-08-22-07-44.png)

### Rose Patterns
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-js-rose-patterns){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-js-rose-patterns/){target=_blank}  

Eine Webseite welche das Zeichnen einer Mandalas erlaubt.
Das Bild wird mit Polarkoordinaten über einen Winkel und Radius implementiert.
Der Radius ist dabei an die Sinus-Funktion gebunden.

!!! info ""
    Damit das Bild nicht sofort übermalen wird muss der Alphakanal des Hintergrundes auf 0 gesetzt werden.

![](./img/2022-01-08-22-11-03.png)

[Mehr Infos :octicons-chevron-right-16:](./projects/rosepattern.md){.md-button .md-btn-small-pad}

### Marching Squares
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-js-marching-squares){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-js-marching-squares/){target=_blank}  

Eine Implementierung des Marching Squares - Algorithmus mit P5 und TypeScript. 
Die Demo visualisiert die Funktion `f(x,y) := sin(x) + sin(y) > t`. Die Demo animiert dabei t zwischen -1 und 1.
Über xCount und yCount kann die Menge an Kacheln erhöht werden. Die Darstellung wird dadurch genauer, braucht aber auch länger zum Rendern.

[Mehr Infos :octicons-chevron-right-16:](./projects/marchingsquares.md){.md-button .md-btn-small-pad}

![](img/2022-01-08-17-25-59.png)
## Templates

### Processing Java Template
![Language](https://img.shields.io/badge/Language-Java-b07219?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/processing-java-template){target=_blank}


Eine Java-Template, mit welcher alle Processing-Funktionen direkt in Java verwendet werden können. Das Repo besitzt zudem eine Gitlab CI/CD Konfiguration welche automatisch einen Build erzeugt. Als Build-Tool wird Maven verwendet.

### TypeScript P5.js Template
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p5.js-typescript-template){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p5.js-typescript-template/){target=_blank}  


Eine TypeScript-Template, welche mit Vite eine statische Seite erzeugt. Die Template verwendet [P5.js](https://p5js.org/){target=_blank} zum Zeichnen. Das Repo besitzt zudem eine Gitlab CI/CD Konfiguration welche automatisch die Seite auf Gitlab Hostet.