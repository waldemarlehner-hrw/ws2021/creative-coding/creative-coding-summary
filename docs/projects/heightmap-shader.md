# Heightmap Shader


![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
![Language](https://img.shields.io/badge/Language-GLSL-086703?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-js-shader-heightmap){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-js-shader-heightmap/){target=_blank}  

------------------------

Eine Implementierung der Heightmap mit TypeScript und P5.js im WebGL-Modus. Es baut auf den Prinzipien aus dem [Heightmap Projekt](./heightmap.md) auf. Dieses sollte am besten zuerst gelesen werden.

Dabei wurde ein Vertex-Shader und eine Heightmap, welche als Textur an den Shader übergeben wurde verwendet.

![](2022-01-24-03-42-51.png)  
Um ein korrektes Bild zu bekommen müssen die erstellten Geometrie-Objekte Quads oder Tris sein. Aus diesem Grund wurden für jede Zeile, wie im Bild gezeigt, die Quads aneinander gereiht übergeben. Dabei befinden sich alle Vertices zwischen (0 <= x <= 1), (0 <= y <= 1). Es handelt sich somit um ein normalisiertes Feld and Vertices.

Im Vertex-Shader wird nun jeder Vertex in Normalized Device Coordinates konvertiert. Für jede Vertex wird auch in der Heightmap der Wert nachgeschaut und die y-Komponente um den gefundenen Wert addiert.

![](2022-01-24-03-51-44.png)

Im Fragment-Shader wird dann lediglich eine in einem Uniform mitgegebene Farbe gesetzt.

Bodenseeregion

![](2022-01-24-03-54-10.png)

![](2022-01-24-03-55-34.png)

Südamerika

![](2022-01-24-03-58-26.png)