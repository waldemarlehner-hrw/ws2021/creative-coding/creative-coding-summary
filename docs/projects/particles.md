# Particles

![Language](https://img.shields.io/badge/Language-Java-b07219?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/particles){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Video-red?style=for-the-badge&logo=youtube)](https://youtu.be/xaRbiuhVwyQ){target=_blank}

------
Erzeugt ein pixeliertes Feuer mithilfe von Partikeln. Die Größe, Farbe und Position hängen von der Lebenszeit des Partikels ab.

Die Implementierung der Partikel wurde dabei sehr Abstrakt gewählt. Das Partikel bekommt bei der Erstellung im Konstruktor Funktionen mitgegeben welche die Farbe, Größe, und Position mitgeben. Somit kann beispielsweise eine andere Farb-Implementierung mitgegeben werden, wodurch das Austauschen einfach möglich ist.

Eine Implementierung welche Feuer darstellt. Die Partikel fallen dabei nach unten:
![](https://i.imgur.com/lHqaf55.png)

Eine Implementierung welche Rauch darstellt. Die Partikel fliegen dabei nach oben:
![](https://i.imgur.com/7i4sVNx.png)
