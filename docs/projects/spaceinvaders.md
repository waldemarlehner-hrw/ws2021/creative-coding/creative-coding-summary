# Space Invaders
![Language](https://img.shields.io/badge/Language-Java-b07219?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/space-invaders){target=_blank}

-------------

Bei diesem Projekt wurde ein Mosaik aus Invadern erzeugt. Das Programm liest ein Eingabe-Bild ein und erzeugt daraus das Ausgabe-Bild.

Das Eingabebild:  
![](./img/2022-01-08-19-34-59.png)

Das Ausgabebild:  
![](./img/2022-01-08-19-35-27.png)

Ein Teilausschnitt:  
![](./img/2022-01-08-19-37-49.png)

Jeder Invader wird aus zwei Farben, der Primär- und Sekundärfarben, erstellt. Für die Farben werden zwei zufällige Pixel aus dem Bild gewählt. Dabei wird nur der Teil des Bildes angeschaut welcher durch den Invader "ersetzt" wird.

Es gab auch Versuche, den Durchschnittswert des Bereichs für die Farbe zu wählen. Dies hatte jedoch [keine guten Ergebnisse](https://i.imgur.com/4U5s303.png){target=_blank}.