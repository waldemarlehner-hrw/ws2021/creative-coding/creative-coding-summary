# Heightmap

![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-heightmap){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-heightmap/){target=_blank}  

------------------------

Die Anwendung erstellt eine Höhenkarte in ein Bild, welches an Joy Divisions "Unknown Pleasures" Album-Cover angelehnt ist.

## Inspiration

![](https://www.udiscover-music.de/wp-content/uploads/2019/06/unknown-pleasures-cover-HEADER.jpg)
Das Album-Cover von Joy Divisions "Unknown Pleasures", welches ans Inspiration herangezogen wurde.

## Eingabe

Die Anwendung verwendet eine Heightmap um Höheninformationen zu bekommen. Hierfür wurde der [Heightmapper](https://tangrams.github.io/heightmapper/#9.46/47.5595/10.5107){target=_blank} verwendet ([Github Repo](https://github.com/tangrams/heightmapper){target=_blank}).

Ein Ausschnitt des Heightmappers:

[![](./img/2022-01-09-01-47-07.png)](https://tangrams.github.io/heightmapper/#10.21/47.6523/9.5747){target=_blank}  
Der Heightmapper exportiert ein Greyscale-Bild, welches als Eingabe verwendet wird.

Das Bild wird in einem vordefinierten Intervall (bspw. alle 10 Pixel) durchgegangen und der Greyscale-Wert rausgelesen und in einem 2-Dimensional Array abgespeichert.

## Ausgabe

Die Werte werden dann als Zeilen nacheinander gezeichnet.  
Hierfür wird der Höhenwert als "Offset" der Zeile verstanden:

![](./img/2022-01-09-02-26-43.png)

Als nächster Schritt wird die ganze Fläche unterhalb der aktuellen Zeile mit der Hintergrundfarbe ausgefüllt (hier grün markiert).

![](./img/2022-01-09-02-29-55.png)

Grund hierfür ist dass es sonst passieren kann dass die vorherige Zeile welche von der aktuellen Zeile verdeckt werden zu sehen sind, was vermieden werden soll:

=== "Ohne Maskierung"

    ![](./img/2022-01-09-02-34-55.png)

=== "Mit Maskierung"

    ![](./img/2022-01-09-02-35-25.png)


Zum Schluss wird die tatsächliche Höhenlinie (hier rot) gezeichnet. 

![](./img/2022-01-09-02-45-43.png)

## Resulat

Die Anwendung wurde auf 3 verschiedenen Höhenkarten laufen gelassen

=== "Bodenseeregion"
    [![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-heightmap/){target=_blank}  
    ![](./img/2022-01-09-03-02-34.png)

=== "Gardasee"
    [![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-heightmap/#garda){target=_blank}  
    ![](./img/2022-01-09-03-05-50.png)
    

=== "Australien"
    [![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-heightmap/#australia){target=_blank}  
    ![](./img/2022-01-09-03-06-17.png)