# Shader Metaballs

![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
![Language](https://img.shields.io/badge/Language-GLSL-086703?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-js-shader-metacircles){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-js-shader-metacircles/){target=_blank}  
-----------


Implementierung von Metaballs mithilfe TypeScript, p5.js und WebGL.  

## Motivation

Nachdem sich die Verwendung von [Marching Squares](./marchingsquares.md) zur Evaluation einer zweidimensionalen Funktion sich als nicht performant genug herausstellte wollte ich die Implementierung von Metabällen mit einem Shader versuchen.

## TLDR: Shader

Kurz (und oberflächlich) zu Shadern:

Es handelt sich hierbei um Programme welche auf den Grafikkarte ausgeführt werden. 
Die CPU ist sehr gut darin, schnell **individuelle** Aufgaben zu lösen.  
Die Grafikkarte ist sehr gut darin, parallelisiert **dieselbe** Aufgabe auf verschiedene Eingaben zu lösen. 

Shader sind in zwei Stufen aufgebaut:  
Zuerst können mit einem Vertex-Shader eingehende Vertices transformiert werden.  
Daraufhin wird ein Fragment-Shader verwendet um jedem Pixel eine Farbe zuzuordnen.

### Verwendung des Fragment-Shader
Das Projekt verwendet den Fragment-Shader um die Metaball-Funktion zu Evaluieren. Ist das Resultat über dem Schwellwert, so wird die Farbe des Fragments gesetzt.
Ist das Resultat unterhalb des Schwellwertes, so wird als Farbe schwarz gesetzt.

## Versionen

[Zu aktueller Version springen :octicons-chevron-down-16:](#v03){.md-button .md-btn-small-pad}

### v0.1

Eine einfache Funktionsevaluation. Hier wurde, wie in der [Marching Squares](./marchingsquares.md) Implementierung, `f(x,y) := sin(x) + sin(y) > target` genommen.  
Man sieht dass das Resultat gegenüber von Marching Squares keine Annäherung braucht und dabei mit vollen 60 FPS läuft.

[![Demo](https://img.shields.io/badge/Demo-Video-red?style=for-the-badge&logo=youtube)](https://youtu.be/dx-vdSp-P_k){target=_blank}  
![](./img/2022-01-09-04-31-46.png)

### v0.2

Hier wurden die tatsächlichen Bälle implementiert. Die Bälle haben jeweils eine Position (x,y), eine Größe und eine Richtung.

Es wurde eine Schwerkraftsimulation implementiert bei welcher sich alle Bälle gegenseitig, je nach dem wie groß sie sind, anziehen.
Die Position, Größe und Farbe aller Bälle werden dem Shader als Float-Buffer übergeben.

Der Shader verwendet die Metaball-Funktion auf dem in `v0.1` geschriebenen Code aufzubauen und zu bestimmen, ob ein Pixel zu einem Metaball gehört.
Die Funktion kann auf [Desmos](https://www.desmos.com/calculator/evgwgorgvj?lang=de){target=_blank} eingesehen werden.


[![Demo](https://img.shields.io/badge/Demo-Video-red?style=for-the-badge&logo=youtube)](https://youtu.be/xWSM-5dV2dc){target=_blank}  
![](./img/2022-01-09-04-47-13.png)
![](./img/2022-01-09-04-52-56.png)

### v0.3

Mit dieser Version wurden nun auch die Farben evaluiert.

Hierfür wurde folgende Funktion verwendet (Pseudocode):
```
wertSumme
farbSumme

für alle Kugeln:
    wertSumme += Wert der Funktion für Kugel
    farbSumme += kugel.farbe * Wert der Funktion für Kugel

farbSumme /= wertSumme
```
!!! tip ""
    Die genaue Implementierung ist [hier](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-js-shader-metacircles/-/blob/v0.3/src/shaders/default.frag#L40-47){target=_blank} zu finden.

[![Demo](https://img.shields.io/badge/Demo-Video-red?style=for-the-badge&logo=youtube)](https://youtu.be/rwe9auFRZo4){target=_blank}  
=== "Resultat"
    ![](./img/2022-01-09-05-01-49.png)

=== "Resultat ohne Schwelle"
    ![](./img/2022-01-09-05-02-07.png)

    Hier wurde die Schwelle so tief gesetzt dass alle Fragments die berechnete Farbe wählen

!!! warning "Probleme mit Farbberechnung"
    Diese Resultate sehen gut aus **wenn die Kreise weit entfernt sind**.  
    Hier ist ein Beispiel wo dies nicht der Fall ist.
    ![](./img/2022-01-09-05-07-20.png) 
