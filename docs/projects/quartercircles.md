# Quarter Circles

![Language](https://img.shields.io/badge/Language-Java-b07219?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/quarter-circles){target=_blank}

------------------

Erzeugt eine Vektorgrafik aus Viertelkreisen.

Dabei wird über die Distance zwischen einer Kachel und dem Mauszeiger die Größe bzw. der Winkel definiert.
Die Farbe wird Zufällig durch das Setzen eines Farbtons (Hue) und einer Abweichung von diesem Farbton definiert.

## Varianten

### Skalierung
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/quarter-circles/-/tree/v0.2-radius){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Video-red?style=for-the-badge&logo=youtube)](https://youtu.be/dJwJlWLW8yE){target=_blank}

![](https://i.imgur.com/2iEArd1.png)
![](https://i.imgur.com/5aYW1H8.png)

### Winkel
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/quarter-circles/-/tree/v0.2-angle){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Video-red?style=for-the-badge&logo=youtube)](https://youtu.be/urWF7S70uYI){target=_blank}

![](https://i.imgur.com/WiP9Vvr.png)  
![](https://i.imgur.com/k9ZVfcg.png)