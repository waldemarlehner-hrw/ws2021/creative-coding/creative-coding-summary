# Marching Squares

![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-js-marching-squares){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-js-marching-squares/){target=_blank}  

-------------

Eine Implementierung des Marching Squares - Algorithmus mit P5 und TypeScript. 
Die Demo visualisiert die Funktion `f(x,y) := sin(x) + sin(y) > t`. Die Demo animiert dabei t zwischen -1 und 1.
Über xCount und yCount kann die Menge an Kacheln erhöht werden. Die Darstellung wird dadurch genauer, braucht aber auch länger zum Rendern.

Als Referenz und Motivation diente folgendes Video:  
[How Computers Draw Weird Shapes (Marching Squares)](https://www.youtube.com/watch?v=6oMZb3yP_H8){target=_blank}

## Über Marching Squares
Marching Squares ist ein Algorithmus mit welchem Formen, welche über eine Zwei-Dimensionale Gleichung definiert sind, approximiert werden können.

Solch eine Gleichung könnte bspw. `sin(x) + sin(y) > a` sein.

Dabei wird die Zeichenfläche in Kachel unterteilt. Je mehr Kachel, desto genauer ist die Darstellung der Gleichung, doch desto länger braucht die Berechnung.
Für jedes Kache wird die Gleichung an den 4 Ecken evaluiert.
Ist das Resultat größer a, so gilt die Ecke als "innerhalb" der Form. 
Ist das Resultat kleiner, so gilt die Ecke als "außerhalb" der Form.

Sind alle Ecken innerhalb der Form, so wird das ganze Kachel "ausgemalt". Sind keine Ecken innerhalb der Form, wird das Kachel übersprungen.
Kommen Ecken innerhalb und außerhalb der Form vor, so wird wie in der Abbildung die Form gezeichnet.

![](https://i.imgur.com/xS8kttY.png)

Diese Berechnungen werden für alle Kacheln ausgeführt. Als Resultat erhält man eine durch die Gleichung definierte Form.

Es folgt ein Beispiel mit `f(x,y) = sin(x)+sin(y) > -0.29`

![](https://i.imgur.com/KAdQ7D1.png)

Der selbe Ausschnitt mit einer geringeren Auflösung, also mit weniger Kacheln:  
![](https://i.imgur.com/3SioriY.png)