# Rose Patterns
![Language](https://img.shields.io/badge/Language-TypeScript-2b7489?style=for-the-badge)
[![Repo](https://img.shields.io/badge/Source_Code-black?style=for-the-badge&logo=gitlab)](https://gitlab.com/waldemarlehner-hrw/ws2021/creative-coding/p-5-js-rose-patterns){target=_blank}  
[![Demo](https://img.shields.io/badge/Demo-Open_In_Browser-orange?style=for-the-badge)](https://waldemarlehner-hrw.gitlab.io/ws2021/creative-coding/p-5-js-rose-patterns/){target=_blank}  

-------

Eine Webseite welche das Zeichnen einer Mandalas erlaubt.
Das Bild wird mit Polarkoordinaten über einen Winkel und Radius implementiert.
Der Radius ist dabei an die Sinus-Funktion gebunden.

Die Parameter können auf der Demoseite über das Konfigurationspanel geändert werden.  
![](./img/2022-01-08-22-21-25.png)

Zu jedem Frame wird das ganze Canvas mit der Hintergrundfarbe gefüllt. Möchte man also, dass das Bild besteht, so sollte der Alpha-Kanal des Hintergrundes auf 0 gesetzt werden.
![](./img/2022-01-08-22-22-47.png)

## Zeichenmethode

Die Zeichnung geschiet mithilfe von Polarkoordinaten.
Dabei wird der aktuelle Winkel (vgl. `Current Angle` im Konfigurationpanel) mit `Degree Multiplier` multipliziert und durch die Sinus-Funktion evaluiert.
Das Resultat ist der Radius an der gegebenen Stelle.

```ts
const degToRad = this.data.currentAngle * (Math.PI/180)

// Effektiver Radius
let r = Math.sin(this.data.degreeMultiplier*degToRad);

// X und Y-koordinaten auf dem Canvas
let x = Math.sin(degToRad) * -this.data.maxRadius * r + this.screenSize / 2;
let y = Math.cos(degToRad) * this.data.maxRadius * r + this.screenSize / 2;
```

## Resultate

![](https://i.imgur.com/cYBhwkJ.png)  
`Degree Multiplier = 0.43`

![](https://i.imgur.com/mU5QpQc.png)  
`Degree Multiplier = 2`

![](https://i.imgur.com/q7PF0bn.png)  
`Degree Multiplier = 3`

![](https://i.imgur.com/zEOutMd.png)  
`Degree Multiplier = 4`

![](https://i.imgur.com/s6jPdCp.png)  
`Degree Multiplier = 5`

![](https://i.imgur.com/8ZZmQj1.png)  
`Degree Multiplier = 1/2`

![](https://i.imgur.com/wvgCAHn.png)  
`Degree Multiplier = 1/3`

![](https://i.imgur.com/Usr9pEq.png)  
`Degree Multiplier = 1/4`

![](https://i.imgur.com/1ANE7Yn.png)  
`Degree Multiplier = π`

